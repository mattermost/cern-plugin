module gitlab.cern.ch/mattermost/cern-plugin

go 1.16

require (
	github.com/mattermost/mattermost-plugin-api v0.0.14
	github.com/mattermost/mattermost-server/v5 v5.34.2
	github.com/pkg/errors v0.9.1
)
