import sortBy from 'lodash.sortby';
import {createSelector} from 'reselect';
import {getMyChannelMemberships} from 'mattermost-redux/selectors/entities/channels';
import {getCurrentTeam, getTeamMemberships} from 'mattermost-redux/selectors/entities/teams';

import {id as pluginId} from './manifest';

const getPluginState = state => state[`plugins-${pluginId}`];
export const isTeamListOpen = state => getPluginState(state).teamListOpen;
export const isChannelListOpen = state => getPluginState(state).channelListOpen;
const _getTeams = state => getPluginState(state).teams;
const _getChannels = state => getPluginState(state).channels;

export const getTeams = createSelector(
  _getTeams,
  getTeamMemberships,
  (teams, members) => {
    if (teams === null) {
      return null;
    }
    return sortBy(teams, t => t.display_name.toLowerCase()).map(t => ({
      ...t,
      joined: members.hasOwnProperty(t.id),
    }));
  }
);

export const getChannels = createSelector(
  _getChannels,
  getMyChannelMemberships,
  (channels, members) => {
    if (channels === null) {
      return null;
    }
    return sortBy(channels, c => c.display_name.toLowerCase()).map(c => ({
      ...c,
      joined: members.hasOwnProperty(c.id),
    }));
  }
);

export const getCurrentTeamDisplayName = state => {
  const team = getCurrentTeam(state);
  return team ? team.display_name : '(no team)';
};
