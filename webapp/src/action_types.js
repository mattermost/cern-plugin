import {id as pluginId} from './manifest';

export default {
  RECEIVED_TEAMS: `${pluginId}_received_teams`,
  RECEIVED_CHANNELS: `${pluginId}_received_channels`,
  OPEN_TEAM_LIST: `${pluginId}_open_team_list`,
  CLOSE_TEAM_LIST: `${pluginId}_close_team_list`,
  OPEN_CHANNEL_LIST: `${pluginId}_open_channel_list`,
  CLOSE_CHANNEL_LIST: `${pluginId}_close_channel_list`,
};
