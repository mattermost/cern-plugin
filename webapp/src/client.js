import request from 'superagent';

import {id as pluginId} from './manifest';

class Client {
  constructor() {
    this.url = `/plugins/${pluginId}/api/v1`;
  }

  getChannels = async team => {
    return this.doGet(`${this.url}/channels`, {team_name: team});
  };

  getTeams = async () => {
    return this.doGet(`${this.url}/teams`);
  };

  joinTeam = async name => {
    return this.doPost(`${this.url}/join-team`, {team_name: name});
  };

  joinChannel = async (team, name) => {
    return this.doPost(`${this.url}/join-channel`, {team_name: team, channel_name: name});
  };

  doGet = async (url, query = {}, headers = {}) => {
    headers['X-Requested-With'] = 'XMLHttpRequest';
    headers['X-Timezone-Offset'] = new Date().getTimezoneOffset();

    try {
      const response = await request
        .get(url)
        .query(query)
        .set(headers)
        .accept('application/json');

      return response.body;
    } catch (err) {
      throw err;
    }
  };

  doPost = async (url, body, headers = {}) => {
    headers['X-Requested-With'] = 'XMLHttpRequest';
    headers['X-Timezone-Offset'] = new Date().getTimezoneOffset();

    try {
      const response = await request
        .post(url)
        .send(body)
        .set(headers)
        .type('application/json')
        .accept('application/json');

      return response.body;
    } catch (err) {
      throw err;
    }
  };

  doDelete = async (url, body, headers = {}) => {
    headers['X-Requested-With'] = 'XMLHttpRequest';
    headers['X-Timezone-Offset'] = new Date().getTimezoneOffset();

    try {
      const response = await request
        .delete(url)
        .send(body)
        .set(headers)
        .type('application/json')
        .accept('application/json');

      return response.body;
    } catch (err) {
      throw err;
    }
  };

  doPut = async (url, body, headers = {}) => {
    headers['X-Requested-With'] = 'XMLHttpRequest';
    headers['X-Timezone-Offset'] = new Date().getTimezoneOffset();

    try {
      const response = await request
        .put(url)
        .send(body)
        .set(headers)
        .type('application/json')
        .accept('application/json');

      return response.body;
    } catch (err) {
      throw err;
    }
  };
}

export default new Client();
