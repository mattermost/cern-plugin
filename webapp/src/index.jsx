import React from 'react';

import {id as pluginId} from './manifest';
import Reducer from './reducers';
import {mainMenuJoinChannelsButtonClicked, mainMenuJoinTeamsButtonClicked} from './actions';
import JoinTeamsModal from './components/JoinTeamsModal';
import JoinChannelsModal from './components/JoinChannelsModal';

export default class Plugin {
  // eslint-disable-next-line no-unused-vars
  initialize(registry, store) {
    registry.registerReducer(Reducer);

    registry.registerRootComponent(JoinTeamsModal);
    registry.registerRootComponent(JoinChannelsModal);
    registry.registerMainMenuAction(
      'Join Restricted Teams',
      () => store.dispatch(mainMenuJoinTeamsButtonClicked()),
      <i className="icon fa fa-lock" />
    );
    registry.registerMainMenuAction(
      'Join Restricted Channels',
      () => store.dispatch(mainMenuJoinChannelsButtonClicked()),
      <i className="icon fa fa-lock" />
    );
  }
}

window.registerPlugin(pluginId, new Plugin());
