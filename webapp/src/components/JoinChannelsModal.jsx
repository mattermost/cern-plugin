import React, {useEffect} from 'react';
import {Alert, ListGroup, ListGroupItem, Modal} from 'react-bootstrap';
import {connect} from 'react-redux';

import {getCurrentTeamDisplayName, getChannels, isChannelListOpen} from '../selectors';
import * as actions from '../actions';

import LoadingSpinner from './LoadingSpinner';

function ChannelItem({name, displayName, group, joined, onClick}) {
  return (
    <ListGroupItem
      action
      title={!joined ? 'Click to join this channel' : 'You are already in this channel'}
      disabled={joined}
      onClick={() => !joined && onClick(name)}
    >
      {displayName} <small>(via {group})</small>
      {joined && <i style={{float: 'right'}} className="fa fa-check" />}
    </ListGroupItem>
  );
}

function JoinChannelsModal({
  channelListOpen,
  teamName,
  channels,
  handleClose,
  fetchChannels,
  joinChannel,
}) {
  useEffect(() => {
    if (!channelListOpen) {
      return;
    }

    fetchChannels();
  }, [channelListOpen, fetchChannels]);

  let body;
  if (channels === null) {
    body = <LoadingSpinner>Retrieving available channels.</LoadingSpinner>;
  } else if (!channels.length) {
    body = <Alert bsStyle="warning">There are no channels with linked groups in this team.</Alert>;
  } else {
    const hasJoinable = channels.some(c => !c.joined);
    body = (
      <>
        <ListGroup>
          {channels.map(c => (
            <ChannelItem
              key={c.id}
              joined={c.joined}
              name={c.name}
              displayName={c.display_name}
              group={c.group}
              onClick={joinChannel}
            />
          ))}
        </ListGroup>
        {hasJoinable && (
          <Alert bsStyle="info">To join a channel, click it in the list above.</Alert>
        )}
      </>
    );
  }

  return (
    <Modal show={channelListOpen} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Join Restricted Channels (in {teamName})</Modal.Title>
      </Modal.Header>
      <Modal.Body>{body}</Modal.Body>
    </Modal>
  );
}

export default connect(
  state => ({
    channelListOpen: isChannelListOpen(state),
    teamName: getCurrentTeamDisplayName(state),
    channels: getChannels(state),
  }),
  {
    handleClose: actions.closeChannelList,
    fetchChannels: actions.fetchChannels,
    joinChannel: actions.joinChannel,
  }
)(JoinChannelsModal);
