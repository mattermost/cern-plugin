import React from 'react';

export default function LoadingSpinner({children}) {
  return (
    <span className={'LoadingSpinner' + (children ? ' with-text' : '')}>
      <span className="fa fa-spinner fa-fw fa-pulse spinner" />
      {children}
    </span>
  );
}
