import React, {useEffect} from 'react';
import {Alert, ListGroup, ListGroupItem, Modal} from 'react-bootstrap';
import {connect} from 'react-redux';

import {getTeams, isTeamListOpen} from '../selectors';
import * as actions from '../actions';

import LoadingSpinner from './LoadingSpinner';

function TeamItem({name, displayName, group, joined, onClick}) {
  return (
    <ListGroupItem
      action
      title={!joined ? 'Click to join this team' : 'You are already a member of this team'}
      disabled={joined}
      onClick={() => !joined && onClick(name)}
    >
      {displayName} <small>(via {group})</small>
      {joined && <i style={{float: 'right'}} className="fa fa-check" />}
    </ListGroupItem>
  );
}

function JoinTeamsModal({teamListOpen, teams, handleClose, fetchTeams, joinTeam}) {
  useEffect(() => {
    if (!teamListOpen) {
      return;
    }

    fetchTeams();
  }, [teamListOpen, fetchTeams]);

  let body;
  if (teams === null) {
    body = <LoadingSpinner>Retrieving available teams.</LoadingSpinner>;
  } else if (!teams.length) {
    body = <Alert bsStyle="warning">There are no teams with linked groups.</Alert>;
  } else {
    const hasJoinable = teams.some(t => !t.joined);
    body = (
      <>
        <ListGroup>
          {teams.map(t => (
            <TeamItem
              key={t.id}
              joined={t.joined}
              name={t.name}
              displayName={t.display_name}
              group={t.group}
              onClick={joinTeam}
            />
          ))}
        </ListGroup>
        {hasJoinable && <Alert bsStyle="info">To join a team, click it in the list above.</Alert>}
      </>
    );
  }

  return (
    <Modal show={teamListOpen} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Join Restricted Teams</Modal.Title>
      </Modal.Header>
      <Modal.Body>{body}</Modal.Body>
    </Modal>
  );
}

export default connect(
  state => ({
    teamListOpen: isTeamListOpen(state),
    teams: getTeams(state),
  }),
  {
    handleClose: actions.closeTeamList,
    fetchTeams: actions.fetchTeams,
    joinTeam: actions.joinTeam,
  }
)(JoinTeamsModal);
