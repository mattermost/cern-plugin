import {getCurrentTeam} from 'mattermost-redux/selectors/entities/teams';

import Client from './client';
import ActionTypes from './action_types';

export function mainMenuJoinTeamsButtonClicked() {
  return async dispatch => {
    dispatch(openTeamList());
  };
}

export function mainMenuJoinChannelsButtonClicked() {
  return async dispatch => {
    dispatch(openChannelList());
  };
}

function openTeamList() {
  return {
    type: ActionTypes.OPEN_TEAM_LIST,
  };
}

function openChannelList() {
  return {
    type: ActionTypes.OPEN_CHANNEL_LIST,
  };
}

export function closeTeamList() {
  return {
    type: ActionTypes.CLOSE_TEAM_LIST,
  };
}

export function closeChannelList() {
  return {
    type: ActionTypes.CLOSE_CHANNEL_LIST,
  };
}

export function fetchTeams() {
  return async dispatch => {
    let data;
    try {
      data = await Client.getTeams();
    } catch (error) {
      return {error};
    }

    dispatch({
      type: ActionTypes.RECEIVED_TEAMS,
      data,
    });

    return {data};
  };
}

export function fetchChannels() {
  return async (dispatch, getState) => {
    const team = getCurrentTeam(getState());
    let data;
    try {
      data = await Client.getChannels(team.name);
    } catch (error) {
      return {error};
    }

    dispatch({
      type: ActionTypes.RECEIVED_CHANNELS,
      data,
    });

    return {data};
  };
}

export function joinTeam(name) {
  return async () => {
    let data;
    try {
      data = await Client.joinTeam(name);
    } catch (error) {
      return {error};
    }

    return {data};
  };
}

export function joinChannel(name) {
  return async (dispatch, getState) => {
    const team = getCurrentTeam(getState());
    let data;
    try {
      data = await Client.joinChannel(team.name, name);
    } catch (error) {
      return {error};
    }

    return {data};
  };
}
