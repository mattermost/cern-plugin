import {combineReducers} from 'redux';
import {TeamTypes} from 'mattermost-redux/action_types';

import ActionTypes from './action_types';

function teamListOpen(state = false, action) {
  switch (action.type) {
    case ActionTypes.OPEN_TEAM_LIST:
      return true;
    case ActionTypes.CLOSE_TEAM_LIST:
      return false;
    default:
      return state;
  }
}

function channelListOpen(state = false, action) {
  switch (action.type) {
    case ActionTypes.OPEN_CHANNEL_LIST:
      return true;
    case ActionTypes.CLOSE_CHANNEL_LIST:
      return false;
    default:
      return state;
  }
}

function teams(state = null, action) {
  switch (action.type) {
    case ActionTypes.RECEIVED_TEAMS:
      return action.data;
    default:
      return state;
  }
}

function channels(state = null, action) {
  switch (action.type) {
    case ActionTypes.RECEIVED_CHANNELS:
      return action.data;
    case TeamTypes.SELECT_TEAM:
      // clear on team change to avoid showing data from wrong team
      return null;
    default:
      return state;
  }
}

export default combineReducers({
  teamListOpen,
  channelListOpen,
  teams,
  channels,
});
