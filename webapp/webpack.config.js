/* eslint-disable import/unambiguous,import/no-commonjs */
/* global require:false, module:false, __dirname:false */

const path = require('path');

module.exports = {
  entry: ['./src/index.jsx'],
  resolve: {
    modules: ['src', 'node_modules'],
    extensions: ['*', '.js', '.jsx'],
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/env', '@babel/react'],
            plugins: [
              '@babel/plugin-proposal-class-properties',
              '@babel/plugin-proposal-object-rest-spread',
            ],
          },
        },
      },
    ],
  },
  externals: {
    'react': 'React',
    'react-dom': 'ReactDOM',
    'redux': 'Redux',
    'react-bootstrap': 'ReactBootstrap',
    'react-redux': 'ReactRedux',
  },
  output: {
    path: path.join(__dirname, '/dist'),
    publicPath: '/',
    filename: 'main.js',
  },
  devtool: 'source-map',
};
