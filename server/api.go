package main

import (
	"encoding/json"
	"net/http"
	"strings"

	"github.com/mattermost/mattermost-server/v5/plugin"
	"github.com/pkg/errors"
)

type APIErrorResponse struct {
	ID         string `json:"id"`
	Message    string `json:"message"`
	StatusCode int    `json:"status_code"`
}

func writeAPIError(p *Plugin, w http.ResponseWriter, msg string, code int, error error) {
	if error != nil {
		p.API.LogError("error during api call", "msg", msg, "error", error.Error())
	}
	b, _ := json.Marshal(&APIErrorResponse{ID: "", Message: msg, StatusCode: code})
	w.WriteHeader(code)
	w.Write(b)

}

func (p *Plugin) ServeHTTP(c *plugin.Context, w http.ResponseWriter, r *http.Request) {
	config := p.getConfiguration()

	if err := config.IsValid(); err != nil {
		http.Error(w, "This plugin is not configured.", http.StatusNotImplemented)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	switch path := r.URL.Path; path {
	case "/api/v1/teams":
		p.getTeams(w, r)
	case "/api/v1/channels":
		p.getChannels(w, r)
	case "/api/v1/join-team":
		if r.Method == "POST" {
			p.joinTeam(w, r)
		}
	case "/api/v1/join-channel":
		if r.Method == "POST" {
			p.joinChannel(w, r)
		}
	default:
		http.NotFound(w, r)
	}
}

type AuthorizedEntity struct {
	Id          string `json:"id"`
	Name        string `json:"name"`
	DisplayName string `json:"display_name"`
	Group       string `json:"group"`
}

func (p *Plugin) getTeams(w http.ResponseWriter, r *http.Request) {
	userId := r.Header.Get("Mattermost-User-ID")
	if userId == "" {
		writeAPIError(p, w, "Not authorized", http.StatusUnauthorized, nil)
		return
	}

	user, err := p.client.User.Get(userId)
	if err != nil {
		writeAPIError(p, w, "invalid user", http.StatusBadRequest, nil)
		return
	}

	groups, err := p.getUserGroups(user)
	if err != nil {
		writeAPIError(p, w, "Could not get groups", http.StatusBadRequest, err)
		return
	}

	userGroups := make(map[string]bool)
	for _, g := range groups {
		userGroups[strings.ToLower(g)] = true
	}

	mapping, appErr := p.getTeamGroups()
	if appErr != nil {
		writeAPIError(p, w, "Error while retrieving teams", http.StatusInternalServerError, errors.New(appErr.Error()))
		return
	}

	authorized := make([]AuthorizedEntity, 0) // using make() to avoid marshalling empty to null
	for team, group := range mapping {
		if _, ok := userGroups[strings.ToLower(group)]; ok {
			authorized = append(authorized, AuthorizedEntity{
				Id:          team.Id,
				Name:        team.Name,
				DisplayName: team.DisplayName,
				Group:       group,
			})
		}
	}

	b, _ := json.Marshal(authorized)
	w.Write(b)
}

func (p *Plugin) getChannels(w http.ResponseWriter, r *http.Request) {
	userId := r.Header.Get("Mattermost-User-ID")
	if userId == "" {
		writeAPIError(p, w, "Not authorized", http.StatusUnauthorized, nil)
		return
	}

	user, err := p.client.User.Get(userId)
	if err != nil {
		writeAPIError(p, w, "invalid user", http.StatusBadRequest, nil)
		return
	}

	teamName := r.FormValue("team_name")
	if teamName == "" {
		writeAPIError(p, w, "team_name missing", http.StatusBadRequest, nil)
		return
	}
	team, err := p.client.Team.GetByName(teamName)
	if err != nil || team.DeleteAt != 0 {
		writeAPIError(p, w, "Invalid team name", http.StatusBadRequest, nil)
		return
	}
	teamMember, err := p.client.Team.GetMember(team.Id, user.Id)
	if err != nil || teamMember.DeleteAt != 0 {
		writeAPIError(p, w, "Not a team member", http.StatusUnauthorized, nil)
		return
	}

	groups, err := p.getUserGroups(user)
	if err != nil {
		writeAPIError(p, w, "Could not get groups", http.StatusBadRequest, err)
		return
	}

	userGroups := make(map[string]bool)
	for _, g := range groups {
		userGroups[strings.ToLower(g)] = true
	}

	mapping, appErr := p.getChannelGroups(team.Id)
	if appErr != nil {
		writeAPIError(p, w, "Error while retrieving channels", http.StatusInternalServerError, errors.New(appErr.Error()))
		return
	}

	authorized := make([]AuthorizedEntity, 0) // using make() to avoid marshalling empty to null
	for channel, group := range mapping {
		if _, ok := userGroups[strings.ToLower(group)]; ok {
			authorized = append(authorized, AuthorizedEntity{
				Id:          channel.Id,
				Name:        channel.Name,
				DisplayName: channel.DisplayName,
				Group:       group,
			})
		}
	}

	b, _ := json.Marshal(authorized)
	w.Write(b)
}

func (p *Plugin) joinTeam(w http.ResponseWriter, r *http.Request) {
	userId := r.Header.Get("Mattermost-User-ID")
	if userId == "" {
		writeAPIError(p, w, "Not authorized", http.StatusUnauthorized, nil)
		return
	}
	user, err := p.client.User.Get(userId)
	if err != nil {
		writeAPIError(p, w, "invalid user", http.StatusBadRequest, nil)
		return
	}

	req := &struct {
		TeamName string `json:"team_name"`
	}{}
	dec := json.NewDecoder(r.Body)
	if err = dec.Decode(&req); err != nil || req.TeamName == "" {
		writeAPIError(p, w, "team_name missing", http.StatusUnprocessableEntity, nil)
		return
	}

	groups, err := p.getUserGroups(user)
	if err != nil {
		writeAPIError(p, w, "Could not get groups", http.StatusBadRequest, err)
		return
	}

	team, err := p.client.Team.GetByName(req.TeamName)
	if err != nil || team.DeleteAt != 0 {
		writeAPIError(p, w, "Invalid team name", http.StatusBadRequest, nil)
		return
	}
	group, appErr := p.getTeamGroup(team.Id)
	if appErr != nil {
		writeAPIError(p, w, "Could not get team group", http.StatusInternalServerError, errors.New(appErr.Error()))
		return
	}
	if group == "" {
		writeAPIError(p, w, "This team does not have a group configured", http.StatusUnauthorized, nil)
		return
	}
	ok := false
	for _, userGroup := range groups {
		if strings.ToLower(userGroup) == strings.ToLower(group) {
			ok = true
			break
		}
	}
	if !ok {
		writeAPIError(p, w, "You are not authorized to join this team", http.StatusUnauthorized, nil)
		return
	}
	if _, err := p.client.Team.CreateMember(team.Id, user.Id); err != nil {
		writeAPIError(p, w, "Creating team member failed", http.StatusInternalServerError, err)
		return
	}

	w.Write([]byte(`{"status": "OK"}`))
}

func (p *Plugin) joinChannel(w http.ResponseWriter, r *http.Request) {
	userId := r.Header.Get("Mattermost-User-ID")
	if userId == "" {
		writeAPIError(p, w, "Not authorized", http.StatusUnauthorized, nil)
		return
	}
	user, err := p.client.User.Get(userId)
	if err != nil {
		writeAPIError(p, w, "invalid user", http.StatusBadRequest, nil)
		return
	}

	req := &struct {
		TeamName    string `json:"team_name"`
		ChannelName string `json:"channel_name"`
	}{}
	dec := json.NewDecoder(r.Body)
	if err = dec.Decode(&req); err != nil || req.TeamName == "" || req.ChannelName == "" {
		writeAPIError(p, w, "team_name or channel_name missing", http.StatusUnprocessableEntity, nil)
		return
	}

	groups, err := p.getUserGroups(user)
	if err != nil {
		writeAPIError(p, w, "Could not get groups", http.StatusBadRequest, err)
		return
	}

	team, err := p.client.Team.GetByName(req.TeamName)
	if err != nil || team.DeleteAt != 0 {
		writeAPIError(p, w, "Invalid team name", http.StatusBadRequest, nil)
		return
	}
	teamMember, err := p.client.Team.GetMember(team.Id, user.Id)
	if err != nil || teamMember.DeleteAt != 0 {
		writeAPIError(p, w, "Not a team member", http.StatusUnauthorized, nil)
		return
	}
	channel, err := p.client.Channel.GetByNameForTeamName(req.TeamName, req.ChannelName, false)
	if err != nil {
		writeAPIError(p, w, "Invalid channel name", http.StatusBadRequest, nil)
		return
	}
	group, appErr := p.getChannelGroup(channel.Id)
	if appErr != nil {
		writeAPIError(p, w, "Could not get channel group", http.StatusInternalServerError, errors.New(appErr.Error()))
		return
	}
	if group == "" {
		writeAPIError(p, w, "This channel does not have a group configured", http.StatusUnauthorized, nil)
		return
	}
	ok := false
	for _, userGroup := range groups {
		if strings.ToLower(userGroup) == strings.ToLower(group) {
			ok = true
			break
		}
	}
	if !ok {
		writeAPIError(p, w, "You are not authorized to join this channel", http.StatusUnauthorized, nil)
		return
	}
	if _, err := p.client.Channel.AddMember(channel.Id, user.Id); err != nil {
		writeAPIError(p, w, "Creating channel member failed", http.StatusInternalServerError, err)
		return
	}

	w.Write([]byte(`{"status": "OK"}`))
}
