package main

import (
	"fmt"
	"strings"

	"github.com/mattermost/mattermost-server/v5/plugin"

	"github.com/mattermost/mattermost-server/v5/model"
)

const COMMAND_HELP = `* |/cern team-group [name]| - Show/set the group who can join the current team. |*| to clear.
* |/cern channel-group [name]| - Show/set the group who can join the current channel. |*| to clear.
* |/cern list-teams| - List all teams and which group can join (system admin only)
* |/cern list-channels| - List all channels of the current team and which group can join (team admin only)`

const (
	MSG_TEAM_GROUP_SET = `Authorized egroup changed to **%s**.
Please keep in mind that none of the users currently in the team will be removed, even if they are not in the group you specified.`
	MSG_TEAM_GROUP_CHANGED = `Authorized egroup changed to **%s** (previously: %s).
Please keep in mind that none of the users currently in the team will be removed, even if they are not in the group you specified.`
	MSG_CHANNEL_GROUP_SET = `Authorized egroup changed to **%s**.
Please keep in mind that none of the users currently in the channel will be removed, even if they are not in the group you specified.`
	MSG_CHANNEL_GROUP_CHANGED = `Authorized egroup changed to **%s** (previously: %s).
Please keep in mind that none of the users currently in the channel will be removed, even if they are not in the group you specified.`
	MSG_GROUP_REMOVED = "Authorized egroup removed (previously: %s)."
)

func getCommand() *model.Command {
	return &model.Command{
		Trigger:          "cern",
		DisplayName:      "CERN",
		Description:      "Integration with CERN.",
		AutoComplete:     true,
		AutoCompleteDesc: "Available commands: help, team-group, channel-group, list-teams, list-channels",
		AutoCompleteHint: "[command]",
	}
}

func (p *Plugin) postCommandResponse(args *model.CommandArgs, text string) {
	post := &model.Post{
		UserId:    p.BotUserID,
		ChannelId: args.ChannelId,
		Message:   text,
	}
	_ = p.API.SendEphemeralPost(args.UserId, post)
}

func (p *Plugin) handleCommandError(args *model.CommandArgs, error *model.AppError) {
	p.API.LogError("command failed", "team_id", args.TeamId, "user_id", args.UserId, "command", args.Command, "error", error.Error())
	post := &model.Post{
		UserId:    p.BotUserID,
		ChannelId: args.ChannelId,
		Message:   "An error occurred while running your command.",
	}
	_ = p.API.SendEphemeralPost(args.UserId, post)
}

func (p *Plugin) executeCommandListTeams(args *model.CommandArgs, param string) {
	if !p.API.HasPermissionTo(args.UserId, model.PERMISSION_MANAGE_SYSTEM) {
		p.postCommandResponse(args, "Nope, you're not a system admin.")
		return
	}
	mapping, err := p.getTeamGroups()
	if err != nil {
		p.handleCommandError(args, err)
		return
	}
	if len(mapping) == 0 {
		p.postCommandResponse(args, "There are no teams with linked groups")
		return
	}
	resp := "###### Teams with linked groups:\n"
	resp += "|Team|Group|\n"
	resp += "|-|-|\n"
	for team, group := range mapping {
		resp += fmt.Sprintf("|%s (%s)|%s|\n", team.Name, team.DisplayName, group)
	}
	p.postCommandResponse(args, resp)
}

func (p *Plugin) executeCommandListChannels(args *model.CommandArgs, param string) {
	team, err := p.API.GetTeam(args.TeamId)
	if err != nil {
		p.handleCommandError(args, err)
		return
	}
	if !p.API.HasPermissionToTeam(args.UserId, args.TeamId, model.PERMISSION_MANAGE_TEAM) {
		p.postCommandResponse(args, "Nope, you're not a team admin.")
		return
	}
	mapping, err := p.getChannelGroups(args.TeamId)
	if err != nil {
		p.handleCommandError(args, err)
		return
	}
	if len(mapping) == 0 {
		p.postCommandResponse(args, "There are no channels with linked groups in this team")
		return
	}
	resp := fmt.Sprintf("###### Channels with linked groups in %s:\n", team.DisplayName)
	resp += "|Channel|Group|\n"
	resp += "|-|-|\n"
	for channel, group := range mapping {
		resp += fmt.Sprintf("|%s (%s)|%s|\n", channel.Name, channel.DisplayName, group)
	}
	p.postCommandResponse(args, resp)
}

func (p *Plugin) executeCommandTeamGroup(args *model.CommandArgs, param string) {
	team, err := p.API.GetTeam(args.TeamId)
	if err != nil || team.DeleteAt != 0 {
		p.handleCommandError(args, err)
		return
	}
	group, err := p.getTeamGroup(args.TeamId)
	if err != nil {
		p.handleCommandError(args, err)
		return
	}
	suffix := ""
	if team.AllowOpenInvite {
		suffix = "\n\n**Note: This team is public; anyone can join regardless of their group**"
	}
	param = strings.TrimSpace(param)
	if param == "" {
		if group == "" {
			p.postCommandResponse(args, "No group configured"+suffix)
			return
		}
		p.postCommandResponse(args, fmt.Sprintf("Members of '%s' can join this team", group)+suffix)
		return
	}
	if !p.API.HasPermissionToTeam(args.UserId, args.TeamId, model.PERMISSION_MANAGE_TEAM) {
		p.postCommandResponse(args, "Only Team Admins can change the group")
		return
	}
	if param == "*" {
		if err := p.setTeamGroup(args.TeamId, ""); err != nil {
			p.handleCommandError(args, err)
			return
		}
		p.postCommandResponse(args, fmt.Sprintf(MSG_GROUP_REMOVED, group))
	} else {
		if err := p.setTeamGroup(args.TeamId, param); err != nil {
			p.handleCommandError(args, err)
			return
		}
		if group == "" {
			p.postCommandResponse(args, fmt.Sprintf(MSG_TEAM_GROUP_SET, param)+suffix)
		} else {
			p.postCommandResponse(args, fmt.Sprintf(MSG_TEAM_GROUP_CHANGED, param, group)+suffix)
		}
	}
}

func (p *Plugin) executeCommandChannelGroup(args *model.CommandArgs, param string) {
	channel, err := p.API.GetChannel(args.ChannelId)
	if err != nil || channel.DeleteAt != 0 {
		p.handleCommandError(args, err)
		return
	}
	group, err := p.getChannelGroup(args.ChannelId)
	if err != nil {
		p.handleCommandError(args, err)
		return
	}
	if channel.Type != model.CHANNEL_OPEN && channel.Type != model.CHANNEL_PRIVATE {
		p.postCommandResponse(args, "Only regular channels support groups")
		return
	}
	suffix := ""
	permission := model.PERMISSION_MANAGE_PRIVATE_CHANNEL_PROPERTIES
	if channel.Type == model.CHANNEL_OPEN {
		suffix = "\n\n**Note: This channel is public; anyone can join regardless of their group**"
		permission = model.PERMISSION_MANAGE_PUBLIC_CHANNEL_PROPERTIES
	}
	param = strings.TrimSpace(param)
	if param == "" {
		if group == "" {
			p.postCommandResponse(args, "No group configured"+suffix)
			return
		}
		p.postCommandResponse(args, fmt.Sprintf("Members of '%s' can join this channel", group)+suffix)
		return
	}
	if !p.API.HasPermissionToChannel(args.UserId, args.ChannelId, permission) {
		p.postCommandResponse(args, "You are not authorized to change the group for this channel")
		return
	}
	if param == "*" {
		if err := p.setChannelGroup(args.ChannelId, ""); err != nil {
			p.handleCommandError(args, err)
			return
		}
		p.postCommandResponse(args, fmt.Sprintf(MSG_GROUP_REMOVED, group))
	} else {
		if err := p.setChannelGroup(args.ChannelId, param); err != nil {
			p.handleCommandError(args, err)
			return
		}
		if group == "" {
			p.postCommandResponse(args, fmt.Sprintf(MSG_CHANNEL_GROUP_SET, param)+suffix)
		} else {
			p.postCommandResponse(args, fmt.Sprintf(MSG_CHANNEL_GROUP_CHANGED, param, group)+suffix)
		}
	}
}

func (p *Plugin) executeCommandHelp(args *model.CommandArgs, param string) {
	text := "###### Mattermost CERN Plugin - Slash Command Help\n" + strings.Replace(COMMAND_HELP, "|", "`", -1)
	p.postCommandResponse(args, text)
}

func (p *Plugin) ExecuteCommand(c *plugin.Context, args *model.CommandArgs) (*model.CommandResponse, *model.AppError) {
	split := strings.SplitN(args.Command, " ", 3)
	command := split[0]
	action := ""
	param := ""
	if len(split) > 1 {
		action = split[1]
	}
	if len(split) > 2 {
		param = split[2]
	}

	if command != "/cern" {
		return &model.CommandResponse{}, nil
	}

	handlers := map[string]func(*model.CommandArgs, string){
		"list-teams":    p.executeCommandListTeams,
		"list-channels": p.executeCommandListChannels,
		"team-group":    p.executeCommandTeamGroup,
		"channel-group": p.executeCommandChannelGroup,
		"help":          p.executeCommandHelp,
		"":              p.executeCommandHelp,
	}

	if handler, ok := handlers[action]; ok {
		handler(args, param)
	} else {
		p.postCommandResponse(args, "Unknown command; use `/cern help` to see a list of valid commands")
	}

	return &model.CommandResponse{}, nil
}
