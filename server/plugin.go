package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"
	"time"

	pluginapi "github.com/mattermost/mattermost-plugin-api"
	"github.com/mattermost/mattermost-server/v5/model"
	"github.com/mattermost/mattermost-server/v5/plugin"

	"github.com/pkg/errors"
)

const (
	USER_GROUPS_KEY   = "_user_groups"
	TEAM_GROUP_KEY    = "_teamgroup"
	CHANNEL_GROUP_KEY = "_channelgroup"
	CERN_BOT_USERNAME = "cern"
	CERN_BOT_NAME     = "CERN Mattermost"
)

type Plugin struct {
	plugin.MattermostPlugin

	BotUserID string

	// configurationLock synchronizes access to the configuration.
	configurationLock sync.RWMutex

	// configuration is the active plugin configuration. Consult getConfiguration and
	// setConfiguration for usage.
	configuration *configuration

	client *pluginapi.Client
}

func (p *Plugin) OnActivate() error {
	config := p.getConfiguration()

	if err := config.IsValid(); err != nil {
		return err
	}

	p.client = pluginapi.NewClient(p.API)

	botId, err := p.client.Bot.EnsureBot(&model.Bot{
		Username:    CERN_BOT_USERNAME,
		DisplayName: CERN_BOT_NAME,
	})
	if err != nil {
		return errors.Wrap(err, "failed to ensure cern bot")
	}
	p.BotUserID = botId

	p.client.SlashCommand.Register(getCommand())

	return nil
}

func (p *Plugin) UserHasLeftTeam(c *plugin.Context, teamMember *model.TeamMember, actor *model.User) {
	config := p.getConfiguration()

	userTeams, err := p.API.GetTeamsForUser(teamMember.UserId)
	if err != nil {
		p.API.LogError("failed to query user teams", "user_id", teamMember.UserId)
		return
	}

	if len(userTeams) == 0 && config.defaultTeamId != "" {
		join := func() {
			if _, err := p.API.CreateTeamMember(config.defaultTeamId, teamMember.UserId); err != nil {
				p.API.LogError("failed to create team member", "team_id", config.defaultTeamId, "user_id", teamMember.UserId, "error", err.Error())
			}
		}

		if teamMember.TeamId == config.defaultTeamId {
			go func() {
				// avoid ugly UI desync (even though it will send the user to the team picker..)
				time.Sleep(3 * time.Second)
				join()
			}()
		} else {
			join()
		}
	}
}

func (p *Plugin) UserHasBeenCreated(c *plugin.Context, user *model.User) {
	config := p.getConfiguration()

	if config.defaultTeamId != "" {
		if _, err := p.API.CreateTeamMember(config.defaultTeamId, user.Id); err != nil {
			p.API.LogError("failed to create team member", "team_id", config.defaultTeamId, "user_id", user.Id, "error", err.Error())
			return
		}
	}
}

func (p *Plugin) ChannelHasBeenCreated(c *plugin.Context, channel *model.Channel) {
	config := p.getConfiguration()

	if config.defaultTeamId == "" || !config.RestrictDefaultTeamChannelCreation || channel.TeamId != config.defaultTeamId {
		return
	}
	if !p.API.HasPermissionToTeam(channel.CreatorId, channel.TeamId, model.PERMISSION_MANAGE_TEAM) {
		if dmChannel, err := p.API.GetDirectChannel(channel.CreatorId, p.BotUserID); err != nil {
			p.API.LogError("couldn't get bot's DM channel", "user_id", channel.CreatorId)
		} else {
			_, _ = p.API.CreatePost(&model.Post{
				UserId:    p.BotUserID,
				ChannelId: dmChannel.Id,
				Message:   "Channel creation in this team is not permitted. The channel will be automatically deleted in a few moments.",
			})
		}
		go func() {
			time.Sleep(1 * time.Second)
			p.API.SendEphemeralPost(channel.CreatorId, &model.Post{
				UserId:    p.BotUserID,
				ChannelId: channel.Id,
				Message:   "Channel creation in this team is not permitted. The channel will be automatically deleted in a few moments.",
			})
			time.Sleep(5 * time.Second)
			channel.Name = "deleted-" + channel.Id
			if _, err := p.API.UpdateChannel(channel); err != nil {
				p.API.LogError("failed to rename channel", "channel_id", channel.Id)
			}
			time.Sleep(1 * time.Second)
			if err := p.API.DeleteChannel(channel.Id); err != nil {
				p.API.LogError("failed to delete channel", "channel_id", channel.Id)
			}
		}()
	}
}

func (p *Plugin) UserHasJoinedTeam(c *plugin.Context, teamMember *model.TeamMember, actor *model.User) {
	config := p.getConfiguration()

	if config.defaultTeamId != "" && teamMember.TeamId == config.defaultTeamId && config.WelcomeMessage != "" {
		dmChannel, err := p.API.GetDirectChannel(teamMember.UserId, p.BotUserID)
		if err != nil {
			p.API.LogError("couldn't get bot's DM channel", "user_id", teamMember.UserId)
			return
		}
		_, _ = p.API.CreatePost(&model.Post{
			UserId:    p.BotUserID,
			ChannelId: dmChannel.Id,
			Message:   config.WelcomeMessage,
		})
	}
}

func (p *Plugin) GetPluginURLPath() string {
	return "/plugins/" + manifest.Id
}

func (p *Plugin) GetPluginURL() string {
	return p.GetSiteURL() + p.GetPluginURLPath()
}

func (p *Plugin) GetSiteURL() string {
	return strings.TrimRight(*p.API.GetConfig().ServiceSettings.SiteURL, "/")
}

type CERNUserInfo struct {
	UserID string
	Groups []string
}

func (p *Plugin) updateUserGroups(user *model.User) error {
	req, err := http.NewRequest("GET", p.configuration.GroupAPIURL, nil)
	if err != nil {
		return err
	}

	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", p.configuration.GroupAPIToken))
	qs := req.URL.Query()
	qs.Add("authdata", *user.AuthData)
	req.URL.RawQuery = qs.Encode()

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	if resp.StatusCode != 200 {
		return errors.New(fmt.Sprintf("Group request returned status %d: %s", resp.StatusCode, string([]byte(body))))
	}

	var groupList []string
	if err = json.Unmarshal(body, &groupList); err != nil {
		return err
	}

	jsonInfo, err := json.Marshal(groupList)
	if err != nil {
		return err
	}

	if err := p.API.KVSetWithExpiry(user.Id+USER_GROUPS_KEY, jsonInfo, 300); err != nil {
		return err
	}

	return nil
}

func (p *Plugin) getCachedUserGroups(user *model.User) ([]string, error) {
	var groupList []string

	if infoBytes, err := p.API.KVGet(user.Id + USER_GROUPS_KEY); err != nil || infoBytes == nil {
		return nil, err
	} else if err := json.Unmarshal(infoBytes, &groupList); err != nil {
		return nil, err
	}

	return groupList, nil
}

func (p *Plugin) getUserGroups(user *model.User) ([]string, error) {
	groups, err := p.getCachedUserGroups(user)
	if err == nil {
		return groups, nil
	}

	err = p.updateUserGroups(user)
	if err != nil {
		return nil, err
	}

	groups, err = p.getCachedUserGroups(user)
	if err != nil {
		return nil, err
	}

	return groups, err
}

func (p *Plugin) setTeamGroup(teamId string, group string) *model.AppError {
	if group == "" {
		return p.API.KVDelete(teamId + TEAM_GROUP_KEY)
	} else {
		return p.API.KVSet(teamId+TEAM_GROUP_KEY, []byte(group))
	}
}

func (p *Plugin) getTeamGroup(teamId string) (string, *model.AppError) {
	groupBytes, err := p.API.KVGet(teamId + TEAM_GROUP_KEY)
	if err != nil {
		return "", err
	}
	return string(groupBytes), nil
}

func (p *Plugin) getTeamGroups() (map[*model.Team]string, *model.AppError) {
	mapping := make(map[*model.Team]string)
	keys, err := p.API.KVList(0, 99999)
	if err != nil {
		return nil, err
	}
	for _, key := range keys {
		if strings.HasSuffix(key, TEAM_GROUP_KEY) {
			teamId := strings.TrimSuffix(key, TEAM_GROUP_KEY)
			team, err := p.API.GetTeam(teamId)
			if err != nil {
				// don't return err in case getting deleted teams becomes an error at some point
				continue
			}
			if team.DeleteAt != 0 {
				continue
			}
			group, err := p.getTeamGroup(teamId)
			if err != nil {
				return nil, err
			}
			mapping[team] = group
		}
	}
	return mapping, nil
}

func (p *Plugin) setChannelGroup(channelId string, group string) *model.AppError {
	if group == "" {
		return p.API.KVDelete(channelId + CHANNEL_GROUP_KEY)
	} else {
		return p.API.KVSet(channelId+CHANNEL_GROUP_KEY, []byte(group))
	}
}

func (p *Plugin) getChannelGroup(channelId string) (string, *model.AppError) {
	groupBytes, err := p.API.KVGet(channelId + CHANNEL_GROUP_KEY)
	if err != nil {
		return "", err
	}
	return string(groupBytes), nil
}

func (p *Plugin) getChannelGroups(teamId string) (map[*model.Channel]string, *model.AppError) {
	mapping := make(map[*model.Channel]string)
	keys, err := p.API.KVList(0, 99999)
	if err != nil {
		return nil, err
	}
	for _, key := range keys {
		if strings.HasSuffix(key, CHANNEL_GROUP_KEY) {
			channelId := strings.TrimSuffix(key, CHANNEL_GROUP_KEY)
			channel, err := p.API.GetChannel(channelId)
			if err != nil {
				// don't return err in case getting deleted channels becomes an error at some point
				continue
			}
			if channel.DeleteAt != 0 {
				continue
			}
			if teamId != "" && channel.TeamId != teamId {
				continue
			}
			group, err := p.getChannelGroup(channelId)
			if err != nil {
				return nil, err
			}
			mapping[channel] = group
		}
	}
	return mapping, nil
}
