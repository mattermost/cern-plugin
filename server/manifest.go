package main

var manifest = struct {
	Id      string
	Version string
}{
	Id:      "cern",
	Version: "0.2.0",
}
